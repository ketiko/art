# Art [![pipeline status](https://gitlab.com/ketiko/art/badges/master/pipeline.svg)](https://gitlab.com/ketiko/art/commits/master) [![coverage report](https://gitlab.com/ketiko/art/badges/master/coverage.svg)](https://gitlab.com/ketiko/art/commits/master)

A tool to measure average response times

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'art', git: 'https://gitlab.com/ketiko/art.git', tag: 'v0.1.0'
```

Then run

    $ bundle install

## Usage

    $ bundle exec art --help

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release:source_control_push`, which will create a git tag for the version, push git commits and tags.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/ketiko/art.
