# Contributing to Art
👍🎉 First off, thanks for taking the time to contribute! 🎉👍

Please submit bugs to [https://gitlab.com/ketiko/art/issues](https://gitlab.com/ketiko/art/issues) or fork and submit a merge request!