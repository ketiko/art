require 'optparse'
require 'uri'

module Art
  module Cli
    # Used to parse command line arguments into valid options
    class Parser
      DEFAULT_URL = 'https://gitlab.com'.freeze
      DEFAULT_DURATION = 60
      URL_DESCRIPTION = "Url to measure, default: #{DEFAULT_URL}".freeze
      DURATION_DESCRIPTION = "Duration in seconds, default: #{DEFAULT_DURATION}".freeze

      # Parses an array in valid options
      # If no options are passed it will use the defaults
      #
      # Valid options include a duration and url
      #
      # @return [url String, duration Integer]
      #
      # Examples:
      #   url, duration = Art::Cli::Parser.parse(['-u', 'http://google.com', '--duration', '60'])
      def self.parse(args)
        options = { url: DEFAULT_URL, duration: DEFAULT_DURATION }

        OptionParser.new do |opts|
          opts.banner = 'Usage: art [options]'
          opts.on('-u URL', '--url=URL', String, URL_DESCRIPTION) do |url|
            raise OptionParser::InvalidArgument, url unless valid_url?(url)
            options[:url] = url
          end
          opts.on('-d DURATION', '--duration=DURATION', Integer, DURATION_DESCRIPTION) do |duration|
            options[:duration] = duration
          end
        end.parse!(args)

        [options[:url], options[:duration]]
      end

      # Checks if a string is a valid url
      #
      # Ensure the string can be parsed into a uri
      # Ensure that uri is http/https not something like file://
      # Ensure the url matches the regex for a URI
      #
      # @return true/false
      def self.valid_url?(url)
        # Unfortunately URI.parse raises an error
        # Typically we do not want to use exceptions for control logic
        parsed = URI.parse(url)
        return false unless parsed.is_a?(URI::HTTP) || parsed.is_a?(URI::HTTPS)
        url =~ URI::DEFAULT_PARSER.make_regexp
      rescue URI::InvalidURIError
        false
      end
    end
  end
end
