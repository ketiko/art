require 'art/cli/parser'
require 'benchmark'
require 'net/http'

module Art
  # Wraps the parsing of the cli args and measuring the response times
  #
  # All input/output should be handled in this class
  # Also all signal handling
  module Cli
    # Parses the command line arguments and executes arg with them
    #
    # Exit cleanly on ctrl+c
    # Exit if any threads raise an exception
    #
    # Outputs the average response time in ms to stdout
    def self.execute(args)
      trap('SIGINT') { exit }
      Thread.abort_on_exception = true

      url, duration = Parser.parse(args)
      response_times = Art.build_response_times(url, duration)
      average = response_times.count.positive? ? response_times.sum.fdiv(response_times.size) : 0
      puts "Average response time: #{average.round(2)}ms"
    end
  end
end
