require 'art/cli'
require 'art/version'

# Utility for getting response times
module Art
  # Measures the response times for a url over a given duration
  # It uses the wait_duration between taking samples
  #
  # It spawns threads to measure the response time in case
  # is url is slow. This allows us to take the correct number
  # of samples over the specified duration.
  #
  # @return Array[Float]
  def self.build_response_times(url, duration)
    response_times = []
    threads = []
    mutex = Mutex.new
    start_time = Time.now
    while (Time.now - start_time).to_i < duration
      threads << Thread.new(response_times, url) do
        time = Art.response_time(url)
        mutex.synchronize { response_times << time } # Block mutex for the shortest time possible
      end

      sleep(wait_duration)
    end
    threads.each(&:join)
    response_times
  end

  # Duration to wait between responses
  #
  # Allow for stubbing the duration in specs
  def self.wait_duration
    10
  end

  # Get the response time in ms for a url
  #
  # @return Float
  def self.response_time(url)
    # TODO: Should we follow redirects or ensure caching is disabled?
    time = Benchmark.measure { Net::HTTP.get(URI(url)) }
    time.total * 1_000
  end
end
