RSpec.describe Art do
  let(:url) { 'http://google.com' }
  let(:duration) { 0 }

  describe '.response_time' do
    subject do
      VCR.use_cassette('art.response_time') do
        Art.response_time(url)
      end
    end

    it { is_expected.to be > 0.0 }
  end

  describe '.build_response_times' do
    subject do
      VCR.use_cassette('art.build_response_times') do
        Art.build_response_times(url, duration)
      end
    end

    before do
      allow(Art).to receive(:wait_duration).and_return(0)
    end

    it { is_expected.to eq([]) }
  end

  describe '.wait_duration' do
    it { expect(Art.wait_duration).to eq(10) }
  end
end
