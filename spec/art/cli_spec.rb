RSpec.describe Art::Cli do
  describe '.execute' do
    subject do
      Art::Cli.execute(args)
    end

    let(:args) { ['-d', duration.to_s, '-u', url] }
    let(:url) { 'https://google.com' }
    let(:duration) { 30 }
    let(:times) { (0..10).map { rand * 10 } }
    let(:average) { times.sum.fdiv(times.size) }

    before do
      allow(Art).to receive(:build_response_times).with(url, duration).and_return(times)
    end

    it { expect { subject }.to output("Average response time: #{average.round(2)}ms\n").to_stdout }
  end
end
