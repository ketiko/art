RSpec.describe Art::Cli::Parser do
  describe '.parse' do
    subject { described_class.parse(args) }

    let(:valid_url) { 'http://google.com' }
    let(:invalid_url) { 'google' }

    let(:valid_duration) { 10 }
    let(:invalid_duration) { 'abc' }

    describe 'url' do
      context 'when -u URL' do
        let(:args) { ['-u', valid_url] }

        it { is_expected.to include(valid_url) }
      end

      context 'when --url=URL' do
        let(:args) { ["--url=#{valid_url}"] }

        it { is_expected.to include(valid_url) }
      end

      context 'when no url option' do
        let(:args) { [] }

        it { is_expected.to include(described_class::DEFAULT_URL) }
      end

      context 'when invalid url' do
        let(:args) { ["--url=#{invalid_url}"] }

        it { expect { subject }.to raise_error(OptionParser::InvalidArgument) }
      end
    end

    describe 'duration' do
      context 'when -d DURATION' do
        let(:args) { ['-d', valid_duration.to_s] }

        it { is_expected.to include(valid_duration) }
      end

      context 'when --duration=DURAITON' do
        let(:args) { ["--duration=#{valid_duration}"] }

        it { is_expected.to include(valid_duration) }
      end

      context 'when no duration option' do
        let(:args) { [] }

        it { is_expected.to include(described_class::DEFAULT_DURATION) }
      end

      context 'when invalid duration' do
        let(:args) { ["--duration=#{invalid_duration}"] }

        it { expect { subject }.to raise_error(OptionParser::InvalidArgument) }
      end
    end

    context 'when invalid arguments' do
      let(:args) { ['--time', '-crazy', 'other', 123] }

      it { expect { subject }.to raise_error(OptionParser::InvalidOption) }
    end
  end

  describe '.valid_url?' do
    subject { described_class.valid_url?(url) }

    context 'when https url' do
      let(:url) { 'https://www.google.com' }

      it { is_expected.to be_truthy }
    end

    context 'when http url' do
      let(:url) { 'http://www.google.com' }

      it { is_expected.to be_truthy }
    end

    context 'when url without subdomain' do
      let(:url) { 'http://google.com' }

      it { is_expected.to be_truthy }
    end

    context 'when only subdomain' do
      let(:url) { 'www.google.com' }

      it { is_expected.to be_falsey }
    end

    context 'when a number' do
      let(:url) { 123 }

      it { is_expected.to be_falsey }
    end
  end
end
